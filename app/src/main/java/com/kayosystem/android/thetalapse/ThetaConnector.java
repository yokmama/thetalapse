package com.kayosystem.android.thetalapse;

import android.os.AsyncTask;
import android.util.Log;

import com.theta360.lib.PtpipInitiator;
import com.theta360.lib.ThetaException;

import java.io.IOException;

/**
 * Created by yokmama on 14/12/22.
 */
public class ThetaConnector extends AsyncTask<Void, String, PtpipInitiator> {
    public static final String THETA_IPADDRESS = "192.168.1.1";
    private static final String TAG = ThetaConnector.class.getSimpleName();

    @Override
    protected PtpipInitiator doInBackground(Void... params) {
        Log.d(TAG, "create PtpipInitiator");
        try {
            PtpipInitiator ptpipInitiator = new PtpipInitiator(THETA_IPADDRESS);

            return ptpipInitiator;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ThetaException e) {
            e.printStackTrace();
        }

        return null;
    }
}
