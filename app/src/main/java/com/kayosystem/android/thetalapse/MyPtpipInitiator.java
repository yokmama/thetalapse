package com.kayosystem.android.thetalapse;

import android.os.Environment;
import android.util.Log;

import com.theta360.lib.PtpipInitiator;
import com.theta360.lib.ThetaException;
import com.theta360.lib.ptpip.eventlistener.PtpipEventListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by yokmama on 14/12/24.
 */
public class MyPtpipInitiator {
    private static final String TAG = MyPtpipInitiator.class.getSimpleName();
    private static File IMAGE_FOLDER = new File(
            Environment.getExternalStorageDirectory(), "data/com.kayosystem.android.thetalapse/image/");

    PtpipInitiator mPtpipInitiator;

    public MyPtpipInitiator(PtpipInitiator ptpipInitiator) {
        mPtpipInitiator = ptpipInitiator;
        init();
    }

    private void init() {
        if (!IMAGE_FOLDER.exists()) {
            IMAGE_FOLDER.mkdirs();
        }
    }

    private void saveFile(byte[] bufs, int count){
        File hoge = new File(IMAGE_FOLDER, String.format("image-%1$04d.jpeg", count));
        try {
            FileOutputStream outputStream = new FileOutputStream(hoge);
            outputStream.write(bufs);
            outputStream.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public void shoot(final int interval, final int maxCount) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    mPtpipInitiator.setTimeLapseInterval(interval*1000); // インターバル撮影間隔: 10000msec
                    mPtpipInitiator.setTimeLapseNumber(maxCount); // インターバル撮影枚数: 10
                    mPtpipInitiator.setStillCaptureMode(PtpipInitiator.DEVICE_PROP_VALUE_TIMELAPSE_CAPTURE_MODE);
                    mPtpipInitiator.initiateOpenCapture(new PtpipEventListener() {
                        int handle=0;
                        int count=0;
                        @Override
                        public void onObjectAdded(int objectHandle) {
                            super.onObjectAdded(objectHandle);
                            Log.d(TAG, "onObjectAdded");
                            handle = objectHandle;
                        }

                        @Override
                        public void onCaptureComplete(int transactionId) {
                            super.onCaptureComplete(transactionId);
                            Log.d(TAG, "onCaptureComplete");
                            try {
                                byte[] bufs = mPtpipInitiator.getObject(handle);

                                saveFile(bufs, count++);
                            } catch (ThetaException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onDevicePropChanged(int devicePropCode) {
                            super.onDevicePropChanged(devicePropCode);
                            Log.d(TAG, "onDevicePropChanged");
                        }

                        @Override
                        public void onEventListenerInterrupted() {
                            super.onEventListenerInterrupted();
                            Log.d(TAG, "onEventListenerInterrupted");
                        }

                        @Override
                        public void onStoreFull(int devicePropCode) {
                            super.onStoreFull(devicePropCode);
                            Log.d(TAG, "onStoreFull");
                        }
                    });
                } catch (ThetaException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void stop(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    mPtpipInitiator.terminateOpenCapture();
                } catch (ThetaException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
