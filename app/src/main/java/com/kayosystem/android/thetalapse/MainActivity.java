package com.kayosystem.android.thetalapse;

import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.theta360.lib.PtpipInitiator;
import com.theta360.lib.ThetaException;
import com.theta360.lib.ptpip.eventlistener.PtpipEventListener;

import java.io.File;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class MainActivity extends ActionBarActivity implements View.OnClickListener {
    public static final String TAG = MainActivity.class.getSimpleName();

    @InjectView(R.id.textTime)
    TextView textTime;

    @InjectView(R.id.buttonRecord)
    Button buttonRecord;

    @InjectView(R.id.buttonPlay)
    Button buttonPlay;

    @InjectView(R.id.editInterval)
    EditText editInterval;

    MyPtpipInitiator mPtpipInitiator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        buttonRecord.setOnClickListener(this);
        buttonPlay.setOnClickListener(this);

        enableUI(false);
        connect();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.buttonRecord) {
            if(mPtpipInitiator!=null) {
                mPtpipInitiator.shoot(getIntraval(), 100);
            }else{
                connect();
            }
        }
    }

    /***
     * 10分未満	1秒に2フレーム	15倍速
     * 10分〜20分	1秒に1フレーム	30倍速
     * 20分〜40分	2秒に1フレーム	60倍速
     * 40分〜1時間20分	4秒に1フレーム	120倍速
     * 1時間20分〜2時間40分	8秒に1フレーム	240倍速
     * @return
     */
    private int getIntraval(){
        try {
            return Integer.parseInt(editInterval.getText().toString());
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    private void connect(){
        ThetaConnector connector = new ThetaConnector() {
            @Override
            protected void onPostExecute(PtpipInitiator ptpipInitiator) {
                super.onPostExecute(ptpipInitiator);
                Log.d(TAG, "create PtpipInitiator:"+ptpipInitiator);
                if(ptpipInitiator!=null) {
                    mPtpipInitiator = new MyPtpipInitiator(ptpipInitiator);
                    enableUI(true);
                }
            }
        };
        connector.execute();
    }

    private void enableUI(boolean b) {
        //buttonRecord.setEnabled(b);
        buttonPlay.setEnabled(b);
    }
}
